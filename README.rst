===========
tripleo-ipa
===========

This repository contains Ansible for configuring the FreeIPA server for TripleO.

Installation
============

.. code-block:: bash

   $ pip install --prefix=/usr ansible-tripleo-ipa-server

Or, if you are installing from source, in the project directory:

.. code-block:: bash

   $ python setup.py install --prefix=/usr
